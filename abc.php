<?php

// Function to Upload file and auto-increment if file already
function file_upload($file_info)
{

    // Get file/image name (with extension)
    $file_name_complete =  $file_info['name'];

    // Extract file extension
    $extension = pathinfo($file_name_complete, PATHINFO_EXTENSION);

    // Extract file name without extension
    $file_name = pathinfo($file_name_complete, PATHINFO_FILENAME);

    // Temp file location
    $file_temp_location =  $file_info['tmp_name'];

    // Save origial file name variable to track while renaming if file already exists
    $file_name_original = $file_name;

    // Increment file name by 1
    $num = 1;

    /**
     * Check if same file name already exists in upload folder, 
     * append increment number to original file name
     **/
    while (file_exists("files/" . $file_name . "." . $extension)) {
        $file_name = (string) $file_name_original . $num;
        $file_name_complete = $file_name . "." . $extension;
        $num++;
    }

    // Upload file in upload folder
    $file_target_location = "files/" . $file_name_complete;
    $file_upload_status = move_uploaded_file($file_temp_location, $file_target_location);

    if ($file_upload_status == true) {
        //echo "Congratulations. File has been uploaded to: $file_target_location";
        return $file_name_complete;
    } else {
        // echo "Error. File uploading failed! Check if 'upload' folder exists with proper permission and Try again.";
        // print_r(error_get_last());
        return false;
    }
}

if (isset($_POST['submit'])) {

    // Check if file is selected to upload, else show error
    if (!empty($_FILES['user_file']['name'])) {
        $file_path = file_upload($_FILES['user_file']);
        echo "<p>File uploaded: $file_path <p>";
    } else {
        echo "<p>No file found! Please upload a file<p>";
    }
}
?>

<!DOCTYPE html>
<html>

<body>
    <form action="" method="post" enctype="multipart/form-data">
        Upload File: <input type="file" name="user_file" required />
        <input type="submit" name="submit">
    </form>
</body>

</html>